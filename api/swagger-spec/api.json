{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "description": "goproxy is a backend for performing http-requests, based on incoming tasks at json format",
    "title": "goproxy",
    "contact": {
      "name": "Max Davidenko",
      "email": "armwebdev@gmail.com"
    },
    "license": {
      "name": "MIT",
      "url": "http://opensource.org/licenses/MIT"
    },
    "version": "0.1.0"
  },
  "basePath": "/",
  "paths": {
    "/tasks": {
      "get": {
        "description": "Returns tasks list optionally with paging",
        "tags": [
          "Proxy"
        ],
        "summary": "Get tasks",
        "operationId": "proxy",
        "parameters": [
          {
            "type": "integer",
            "description": "rows per page in result set",
            "name": "rowsPerPage",
            "in": "query"
          },
          {
            "type": "integer",
            "description": "page in result set",
            "name": "page",
            "in": "query"
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/tasksListResponse"
          },
          "404": {
            "$ref": "#/responses/notFoundError"
          }
        }
      },
      "post": {
        "description": "Creates new task for http client with specified parameters",
        "tags": [
          "Proxy"
        ],
        "summary": "Create new task",
        "operationId": "proxy",
        "parameters": [
          {
            "description": "task parameters",
            "name": "task",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/createTaskRequest"
            }
          },
          {
            "type": "boolean",
            "description": "run task in async mode",
            "name": "async",
            "in": "query"
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/definitions/Task"
          },
          "400": {
            "$ref": "#/responses/badRequestError"
          }
        }
      }
    },
    "/tasks/{id}": {
      "get": {
        "description": "Returns task with specified id",
        "tags": [
          "Proxy"
        ],
        "summary": "Get task by id",
        "operationId": "proxy",
        "parameters": [
          {
            "type": "string",
            "description": "task id",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/definitions/Task"
          },
          "404": {
            "$ref": "#/responses/notFoundError"
          }
        }
      },
      "delete": {
        "description": "Deletes task with specified id",
        "tags": [
          "Proxy"
        ],
        "summary": "Delete task by id",
        "operationId": "proxy",
        "parameters": [
          {
            "type": "string",
            "description": "task id",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "204": {
            "$ref": "#/responses/noContentResponse"
          },
          "404": {
            "$ref": "#/responses/notFoundError"
          }
        }
      }
    }
  },
  "definitions": {
    "Task": {
      "description": "Task represents task for performing http-request from client",
      "type": "object",
      "properties": {
        "ID": {
          "type": "string"
        },
        "Method": {
          "type": "string"
        },
        "RequestBody": {
          "type": "array",
          "items": {
            "type": "integer",
            "format": "uint8"
          }
        },
        "RequestHeaders": {
          "type": "object",
          "additionalProperties": {
            "type": "string"
          }
        },
        "ResponseBody": {
          "type": "array",
          "items": {
            "type": "integer",
            "format": "uint8"
          }
        },
        "ResponseBodySize": {
          "type": "integer",
          "format": "int64"
        },
        "ResponseHeaders": {
          "type": "object",
          "additionalProperties": {
            "type": "string"
          }
        },
        "Status": {
          "type": "integer",
          "format": "int64"
        },
        "URL": {
          "type": "string"
        }
      },
      "x-go-package": "bitbucket.org/max_davidenko/goproxy/internal/model"
    },
    "createTaskRequest": {
      "description": "Create task request",
      "type": "object",
      "properties": {
        "Method": {
          "type": "string"
        },
        "RequestBody": {
          "type": "array",
          "items": {
            "type": "integer",
            "format": "uint8"
          }
        },
        "RequestHeaders": {
          "type": "object",
          "additionalProperties": {
            "type": "string"
          }
        },
        "URL": {
          "type": "string"
        }
      },
      "x-go-package": "bitbucket.org/max_davidenko/goproxy/api/swagger"
    },
    "tasksListResponse": {
      "description": "Tasks list",
      "type": "object",
      "properties": {
        "Body": {
          "description": "Array of task models\nin:body",
          "type": "object",
          "properties": {
            "tasks": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Task"
              },
              "x-go-name": "Tasks"
            },
            "total_tasks": {
              "type": "integer",
              "format": "int64",
              "x-go-name": "TotalTasks"
            }
          }
        }
      },
      "x-go-package": "bitbucket.org/max_davidenko/goproxy/api/swagger"
    }
  },
  "responses": {
    "badRequestError": {
      "description": "Bad Request",
      "schema": {
        "type": "object",
        "properties": {
          "message": {
            "description": "Detailed error message",
            "type": "string",
            "x-go-name": "Message"
          }
        }
      }
    },
    "noContentResponse": {
      "description": "An empty response for no content"
    },
    "notFoundError": {
      "description": "Not Found",
      "schema": {
        "type": "object",
        "properties": {
          "message": {
            "description": "Detailed error message",
            "type": "string",
            "x-go-name": "Message"
          }
        }
      }
    },
    "tasksListResponse": {
      "description": "Tasks list",
      "schema": {
        "type": "object",
        "properties": {
          "tasks": {
            "type": "array",
            "items": {
              "$ref": "#/definitions/Task"
            },
            "x-go-name": "Tasks"
          },
          "total_tasks": {
            "type": "integer",
            "format": "int64",
            "x-go-name": "TotalTasks"
          }
        }
      }
    }
  }
}