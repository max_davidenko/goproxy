package swagger

import "bitbucket.org/max_davidenko/goproxy/internal/model"

// Tasks list
// swagger:response tasksListResponse
type tasksListResponse struct {
	// Array of task models
	// in:body
	Body struct {
		TotalTasks int           `json:"total_tasks"`
		Tasks      []*model.Task `json:"tasks"`
	}
}

// Create task request
// swagger:model createTaskRequest
type createTaskRequest struct {
	Method string
	URL string
	RequestHeaders   map[string]string
	RequestBody      []byte
}
