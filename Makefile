all: api-docs build
build:
	go build -o bin/goproxy cmd/goproxy/main.go
install:
	go install ./cmd/goproxy
run:
	./bin/goproxy
api-docs:
	cd cmd/goproxy && swagger generate spec -o ../../api/swagger-spec/api.json --scan-models && \
	cp ../../api/swagger-spec/api.json ../../third_party/swagger-ui/swagger.json && \
	cd ../../third_party && go-bindata-assetfs -pkg=swaggerui -o ../web/assets.go swagger-ui/...
