// goproxy
//
// goproxy is a backend for performing http-requests, based on incoming tasks at json format
//
//     Schemes: http
//     BasePath: /
//     Version: 0.1.0
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: Max Davidenko <armwebdev@gmail.com>
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//
// swagger:meta
package main

import (
	_ "bitbucket.org/max_davidenko/goproxy/api/swagger"
	"bitbucket.org/max_davidenko/goproxy/internal/controller"
	"bitbucket.org/max_davidenko/goproxy/internal/repository"
	"bitbucket.org/max_davidenko/goproxy/internal/router"
	"bitbucket.org/max_davidenko/goproxy/web"
	"flag"
	"log"
	"net/http"
	"os"
	"strconv"
)

const (
	defaultHttpPort     = 8001
	defaultWorkersCount = 10
)

func main() {
	httpPort := flag.Int("port", defaultHttpPort, "Service  port (8001)")
	workersCount := flag.Int("workers", defaultWorkersCount, "Workers count for execution client requests (10)")

	help := flag.Bool("help", false, "Prints this help")

	flag.Parse()

	if *help {
		flag.PrintDefaults()
		os.Exit(0)
	}

	repoParams := map[string]string{
		"repoName": "default",
	}

	repo, err := repository.CreateRepository(repoParams)
	if err != nil {
		log.Fatal("Repository creation error: " + err.Error())
	}

	err = repo.InitRepository()
	if err != nil {
		log.Fatal("Model initialization error: " + err.Error())
	}

	appController := controller.NewController(repo, *workersCount)
	appRouter := router.NewRouter(appController)

	appRouter.PathPrefix("/swagger-ui/").
		Handler(http.StripPrefix("/swagger-ui/", swaggerui.GetFS()))

	appRouter.PathPrefix("/").
		Handler(http.StripPrefix("/", swaggerui.GetFS()))

	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(*httpPort), appRouter))
}
