package controller

type apiError struct {
	Message string `json:"message"`
}

type badRequestError struct {
	message string
}

func (bre badRequestError) Error() string {
	return bre.message
}

type notFoundError struct {
	message string
}

func (nfe notFoundError) Error() string {
	return nfe.message
}

type internalError struct {
	message string
}

func (ie internalError) Error() string {
	return ie.message
}

func newApiError(err error) apiError {
	return apiError{Message: err.Error()}
}

func newApiErrorFromString(message string) apiError {
	return apiError{Message: message}
}

func newBadRequestError(err error) badRequestError {
	return badRequestError{err.Error()}
}

func newBadRequestErrorFromString(message string) badRequestError {
	return badRequestError{message}
}

func newNotFoundError(err error) notFoundError {
	return notFoundError{err.Error()}
}

func newNotFoundErrorFromString(message string) notFoundError {
	return notFoundError{message}
}

func newInternalError(err error) internalError {
	return internalError{err.Error()}
}

func newInternalErrorFromString(message string) internalError {
	return internalError{message}
}
