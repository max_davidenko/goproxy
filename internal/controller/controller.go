package controller

import (
	"bitbucket.org/max_davidenko/goproxy/internal/model"
	"bitbucket.org/max_davidenko/goproxy/internal/repository"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
)

// Controller contains link to instance of application's repository and methods responsible for incoming requests handling
type Controller struct {
	repo repository.Repository
	tasksChannel chan *model.Task
}

// NewController creates new Controller instance with specified repository implementation
func NewController(repo repository.Repository, workersCount int) *Controller {
	channel := make(chan *model.Task)
	ctrl := &Controller{repo, channel}
	if workersCount <= 0 {
		workersCount = 10
	}

	for i := 0; i < workersCount; i++ {
		go ctrl.asyncTaskInvoke(channel)
	}

	return ctrl
}

// Handlers returns bindings for paths and handler functions
func (ctrl *Controller) Handlers() map[string]http.HandlerFunc {
	bindings := make(map[string]http.HandlerFunc)
	if ctrl == nil {
		return bindings
	}

	bindings["Get tasks"] = ctrl.GetTasks
	bindings["Get task"] = ctrl.GetTask
	bindings["Create task"] = ctrl.CreateTask
	bindings["Delete task"] = ctrl.DeleteTask

	return bindings
}

func writeBasicHeaders(rw http.ResponseWriter) {
	rw.Header().Set("Content-Type", "application/json; charset=UTF-8")
	rw.Header().Set("Access-Control-Allow-Origin", "*")
}

func writeResponse(rw http.ResponseWriter, bodyObj interface{}) {
	body, jsErr := json.Marshal(bodyObj)
	if jsErr != nil {
		writeError(rw, jsErr)
	} else {
		rw.WriteHeader(http.StatusOK)
		rw.Write(body)
	}
}

func writeError(rw http.ResponseWriter, err error) {
	errBody, jsErr := json.Marshal(newApiError(err))
	if jsErr != nil {
		rw.WriteHeader(http.StatusInternalServerError)
	} else {
		switch err.(type) {
		case badRequestError:
			rw.WriteHeader(http.StatusBadRequest)
		case notFoundError:
			rw.WriteHeader(http.StatusNotFound)
		default:
			rw.WriteHeader(http.StatusInternalServerError)
		}

		rw.Write(errBody)
	}
}

func unmarshalBody(request *http.Request, response http.ResponseWriter, out interface{}) bool {
	body, err := ioutil.ReadAll(io.LimitReader(request.Body, 1048576))

	if err != nil {
		writeError(response, newInternalErrorFromString("Unable to read request body: "+err.Error()))
		return false
	}

	if err := request.Body.Close(); err != nil {
		writeError(response, newInternalErrorFromString("Unable to close request body: "+err.Error()))
		return false
	}

	if err := json.Unmarshal(body, out); err != nil {
		writeError(response, newInternalErrorFromString("Unable to unmarshal body: "+err.Error()))
		return false
	}

	return true
}
