package controller

import (
	"bitbucket.org/max_davidenko/goproxy/internal/model"
	"bitbucket.org/max_davidenko/goproxy/internal/repository"
	"bytes"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/satori/go.uuid"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

type tasksListResponse struct {
	TotalTasks int           `json:"total_tasks"`
	Page int				`json:"page"`
	Tasks      []*model.Task `json:"tasks"`
}

// GetTasks is a handler for GET /tasks
func (ctrl *Controller) GetTasks(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	var err error
	rowsPerPage, page := 0, 0

	// Allow invalid paging parameters, if so - paging will not be used
	rowsPerPageStr, ok := request.URL.Query()["rowsPerPage"]
	if ok {
		rowsPerPage, _ = strconv.Atoi(rowsPerPageStr[0])
	}
	pageStr, ok := request.URL.Query()["page"]
	if ok {
		page, _ = strconv.Atoi(pageStr[0])
	}

	tasks, total, err := ctrl.repo.GetTasks(rowsPerPage, page)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	tasksList := tasksListResponse{TotalTasks: total, Tasks: tasks, Page: page}
	writeResponse(response, tasksList)
}

// GetTask is a handler for GET /tasks/{id}
func (ctrl *Controller) GetTask(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	taskId, ok := mux.Vars(request)["id"]
	if !ok {
		writeError(response, newBadRequestErrorFromString("task id not specified"))
		return
	}

	task, err := ctrl.repo.GetTask(taskId)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}
	writeResponse(response, task)
}

// CreateTask is a handler for POST /tasks
func (ctrl *Controller) CreateTask(response http.ResponseWriter, request *http.Request) {
	task := &model.Task{}
	if !unmarshalBody(request, response, task) {
		return
	}

	task.Method = strings.TrimSpace(task.Method)
	if task.Method == "" {
		writeError(response, newBadRequestErrorFromString("request method must be specified"))
		return
	}
	task.Method = strings.ToUpper(task.Method)
	if task.Method != "GET" && task.Method != "POST" && task.Method != "PUT" && task.Method != "DELETE" {
		writeError(response, newBadRequestErrorFromString("request method not supported. Allowed methods: GET, POST, PUT, DELETE"))
		return
	}

	task.URL = strings.TrimSpace(task.URL)
	if task.URL == "" {
		writeError(response, newBadRequestErrorFromString("request URL must be specified"))
		return
	}

	id := uuid.NewV4()

	task.ID = id.String()
	if err := ctrl.repo.CreateTask(task); err != nil {
		writeError(response, err)
		return
	}

	async := false
	asyncStr, ok := request.URL.Query()["async"]
	if ok {
		async = strings.EqualFold(asyncStr[0], "true")
	}

	if async {
		ctrl.tasksChannel <- task
	} else {
		if err := invoke(task); err != nil {
			writeError(response, err)
			return
		}
		if err := ctrl.repo.UpdateTask(task); err != nil {
			writeError(response, err)
			return
		}
	}

	writeResponse(response, task)
}

// DeleteTask is a handler for DELETE /tasks/{id}
func (ctrl *Controller) DeleteTask(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	taskId, ok := mux.Vars(request)["id"]
	if !ok {
		writeError(response, newBadRequestErrorFromString("task id not specified"))
		return
	}

	task, err := ctrl.repo.GetTask(taskId)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	err = ctrl.repo.DeleteTask(task)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

func invoke(task *model.Task) error {
	client := &http.Client{}
	var body io.Reader = nil
	if task.RequestBody != nil && len(task.RequestBody) > 0 {
		body = bytes.NewReader(task.RequestBody)
	}
	req, err := http.NewRequest(task.Method, task.URL, body)
	if err != nil {
		return err
	}
	if task.RequestHeaders != nil && len(task.RequestHeaders) > 0 {
		for header, val := range task.RequestHeaders {
			req.Header.Set(header, val)
		}
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	task.Status = resp.StatusCode
	task.ResponseBody, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	task.ResponseBodySize = len(task.ResponseBody)

	task.ResponseHeaders = make(map[string]string)
	for header, val := range resp.Header {
		task.ResponseHeaders[header] = val[0]
	}

	return nil
}

func (ctrl *Controller) asyncTaskInvoke(channel chan *model.Task) {
	for task := range channel {
		if err := invoke(task); err != nil {
			fmt.Println(err)
			task.Status = -1
		}
		ctrl.repo.UpdateTask(task)
	}
}