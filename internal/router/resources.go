package router

// Resource represents http endpoint specification
type Resource struct {
	Name   string
	Method string
	Path   string
}

type Resources []Resource

var resources = Resources{

	// swagger:operation GET /tasks Proxy proxy
	// ---
	// summary: Get tasks
	// description: Returns tasks list optionally with paging
	// parameters:
	// - name: rowsPerPage
	//   in: query
	//   description: rows per page in result set
	//   type: integer
	//   required: false
	// - name: page
	//   in: query
	//   description: page in result set
	//   type: integer
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/tasksListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Get tasks",
		"GET",
		"/tasks",
	},

	// swagger:operation GET /tasks/{id} Proxy proxy
	// ---
	// summary: Get task by id
	// description: Returns task with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: task id
	//   type: string
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Task"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Get task",
		"GET",
		"/tasks/{id}",
	},

	// swagger:operation POST /tasks Proxy proxy
	// ---
	// summary: Create new task
	// description: Creates new task for http client with specified parameters
	// parameters:
	// - name: task
	//   in: body
	//   description: task parameters
	//   schema:
	//     "$ref": "#/definitions/createTaskRequest"
	//   required: true
	// - name: async
	//   in: query
	//   description: run task in async mode
	//   type: boolean
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Task"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Create task",
		"POST",
		"/tasks",
	},

	// swagger:operation DELETE /tasks/{id} Proxy proxy
	// ---
	// summary: Delete task by id
	// description: Deletes task with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: task id
	//   type: string
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete task",
		"DELETE",
		"/tasks/{id}",
	},
}
