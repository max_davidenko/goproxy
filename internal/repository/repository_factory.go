package repository

import (
	"errors"
	"strings"
)

// CreateRepository creates new repository with specified params
func CreateRepository(params map[string]string) (Repository, error) {
	repoName := params["repoName"]
	if strings.EqualFold(repoName, "default") {
		return newDefaultRepository(params)
	}

	return nil, errors.New("unknown repository name or name is not specified")
}

// newDefaultRepository - creates new instance of default repository
func newDefaultRepository(_ map[string]string) (Repository, error) {
	repo := &DefaultRepository{}
	if err := repo.InitRepository(); err != nil {
		return nil, err
	}
	return repo, nil
}
