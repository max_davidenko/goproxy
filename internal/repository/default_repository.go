package repository

import (
	"bitbucket.org/max_davidenko/goproxy/internal/model"
	"sort"
	"sync"
)

type tasksStorage struct {
	mutex   sync.RWMutex
	storage map[string]model.Task
}

func newTasksStorage() *tasksStorage {
	return &tasksStorage{
		storage: make(map[string]model.Task),
	}
}

func (ts *tasksStorage) get(key string) (model.Task, bool) {
	ts.mutex.RLock()
	val, ok := ts.storage[key]
	ts.mutex.RUnlock()
	return val, ok
}

func (ts *tasksStorage) put(key string, value model.Task) {
	ts.mutex.Lock()
	ts.storage[key] = value
	ts.mutex.Unlock()
}

func (ts *tasksStorage) delete(key string) {
	ts.mutex.Lock()
	delete(ts.storage, key)
	ts.mutex.Unlock()
}

func (ts *tasksStorage) keys() []string {
	keys := []string{}
	ts.mutex.RLock()
	for key := range ts.storage {
		keys = append(keys, key)
	}
	ts.mutex.RUnlock()
	if len(keys) > 0 {
		sort.Strings(keys)
	}
	return keys
}

func (ts *tasksStorage) getRange(keys []string) []model.Task {
	if keys == nil || len(keys) == 0 {
		return nil
	}

	values := []model.Task{}
	ts.mutex.RLock()
	for _, key := range keys {
		value, ok := ts.storage[key]
		if ok {
			values = append(values, value)
		}
	}
	ts.mutex.RUnlock()

	return values
}

// DefaultRepository implements repository interface with map as storage
type DefaultRepository struct {
	db *tasksStorage
}

// InitRepository performs default repository initialization
func (repo *DefaultRepository) InitRepository() error {
	repo.db = newTasksStorage()
	return nil
}

// CreateTask creates new task entry at repository
func (repo *DefaultRepository) CreateTask(task *model.Task) error {
	repo.db.put(task.ID, *task)
	return nil
}

// UpdateTask updates specified task entry at repository
func (repo *DefaultRepository) UpdateTask(task *model.Task) error {
	return repo.CreateTask(task)
}

// DeleteTask deleted specified task entry from repository
func (repo *DefaultRepository) DeleteTask(task *model.Task) error {
	repo.db.delete(task.ID)
	return nil
}

// GetTask returns task entry with specified id from repository
func (repo *DefaultRepository) GetTask(id string) (*model.Task, error) {
	task, found := repo.db.get(id)
	if !found {
		return nil, &NotFoundError{"task"}
	}

	return &task, nil
}

// GetTasks returns tasks from repository with specified paging parameters
func (repo *DefaultRepository) GetTasks(rowsPerPage int, page int) ([]*model.Task, int, error) {
	keys := repo.db.keys()
	if len(keys) == 0 {
		return nil, 0, nil
	}
	totalKeys := len(keys)

	tasks := []model.Task{}
	if rowsPerPage > totalKeys || rowsPerPage <= 0 || page <= 0 {
		tasks = repo.db.getRange(keys)
	} else {
		pagesCount := totalKeys / rowsPerPage
		if totalKeys%rowsPerPage != 0 {
			pagesCount++
		}
		if page > pagesCount {
			tasks = repo.db.getRange(keys[totalKeys-rowsPerPage:])
		} else {
			if page == pagesCount {
				tasks = repo.db.getRange(keys[(page-1)*rowsPerPage:])
			} else {
				tasks = repo.db.getRange(keys[(page-1)*rowsPerPage : page*rowsPerPage])
			}
		}
	}

	if len(tasks) > 0 {
		result := []*model.Task{}
		for _, task := range tasks {
			result = append(result, &task)
		}
		return result, totalKeys, nil
	}

	return nil, 0, &NotFoundError{"tasks"}
}
