package repository

import "fmt"

type NotFoundError struct {
	What string
}

func (nfe NotFoundError) Error() string {
	return fmt.Sprintf("%s not found", nfe.What)
}
