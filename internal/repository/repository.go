package repository

import "bitbucket.org/max_davidenko/goproxy/internal/model"

type Repository interface {
	InitRepository() error
	CreateTask(*model.Task) error
	UpdateTask(*model.Task) error
	DeleteTask(*model.Task) error
	GetTask(string) (*model.Task, error)
	GetTasks(int, int) ([]*model.Task, int, error)
}
