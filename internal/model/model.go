package model

// Task represents task for performing http-request from client
type Task struct {
	ID               string
	Method           string
	URL              string
	RequestHeaders   map[string]string
	RequestBody      []byte
	Status           int
	ResponseBodySize int
	ResponseHeaders  map[string]string
	ResponseBody     []byte
}
